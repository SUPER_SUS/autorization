<?php

/** Авторизация на PHP и MySQL */
session_start();
error_reporting(0);
$mysqlUser = 'root';
$mysqlPwd = 'root';
$mysqlDbName = 'rabota';

/** Делаем подключение к MySQL */
$mysqli = mysqli_connect('localhost', $mysqlUser, $mysqlPwd, $mysqlDbName);

/** Выводим любую ошибку соединения */
if ($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}

$login = $_POST['login'] ?? '';
$password = $_POST['password'] ?? '';

/** Переменная для ошибки */
$errorMessage = '';


/** Приверяем пришел ли запрос */
if($_POST) {
    /** Проверяем отправил ли пользователь данные */
    if($login && $password) {
        $authSuccess = false;
        $results = $mysqli->query('SELECT * FROM `users`');
        while($user=$results->fetch_object() ) {
            if($login == $user->login && $password == $user->password) {
                $authSuccess = true;
                  $_SESSION['user']['id'] = $user->id;
                  break;
            }
        }

        if($authSuccess) {
          
        }
        else{
            $errorMessage = 'Вы не авторизовались';
        }
    }
    else {
        $errorMessage = 'Не переданы нужные данные: логин или пароль';
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8"> 
        <link rel="stylesheet" type="text/css" href="./css/style.css">

        <title> Авторизация </title>
    </head>
    <body>
        <div class="container">
            <?php if($errorMessage): ?>
                <p><center style="color: red;"><?= $errorMessage ?></center></p>
            <?php endif; ?>
            <?php if($_SESSION['user']['id']??0): ?>
                <p ><center  style="color: green;">Вы авторизовались</center></p>
            <?php endif; ?>
            <form method="post" action="">
                <p> Логин: <input name="login" type="text"></p>
                <p> Пароль: <input name="password" type="password"></p>
                <p> <center><button  type="submit">Войти</button></center> </p>
            </form>
        </div>
         </div>
    </body>
</html>
